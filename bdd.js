const express = require('express')
const app = express()
var mysql = require('mysql');

const port = process.env.port || 8080 

app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))

//init BDD
var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "mydb"
});

//Permet de créer la DATABASE
con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
    con.query("CREATE DATABASE IF NOT EXISTS mydb", function (err, result) {
      if (err) throw err;
      console.log("Database created");
    });   
});

//GET ALL avec une LIMIT et un OFFSET et choisir ORDER et Croissant ou decroissant
app.get('/users', function(req, res) {
  if(typeof req.query.limit !== 'undefined' && req.query.offset !== 'undefined'){
  let sql = `SELECT * FROM users LIMIT ${req.query.limit} OFFSET ${req.query.offset}`;
  con.query(sql, function(err, data, fields) {
    if (err) throw err;
    res.json({
      status: 200,
      data,
      message: `Tous les utilisateurs sont affiché à partir du ${req.query.offset} et vous avez ${req.query.limit} utilisateur affiché`
    })
    console.log(data)
  })
  }else if(req.query.order !== 'undefined' && req.query.reverse == 'false'){
    let sql = `SELECT * FROM users ORDER BY ${req.query.order}`;
    con.query(sql, function(err, data, fields) {
      if (err) throw err;
      res.json({
        status: 200,
        data,
        message: `Tous les utilisateurs sont affiché par ordre croissant par rapport au ${req.query.order}`
      })
      console.log(data)
    })
    }else if(req.query.order !== 'undefined' && req.query.reverse == 'true'){
      let sql = `SELECT * FROM users ORDER BY ${req.query.order} DESC`;
      con.query(sql, function(err, data, fields) {
        if (err) throw err;
        res.json({
          status: 200,
          data,
          message: `Tous les utilisateurs sont affiché par ordre décroissant par rapport au ${req.query.order}`
        })
        console.log(data)
      })
      }else{
        let sql = `SELECT * FROM users`;
        con.query(sql, function(err, data, fields) {
          if (err) throw err;
          res.json({
            status: 200,
            data,
            message: "Tous les utilisateurs sont affiché"
          })
          console.log(data)
        })
      }
});

//GET ID
  app.get('/users/:id', function(req, res) {
  let sql = `SELECT * FROM users WHERE id = ${req.params.id}`;
  con.query(sql, function(err, data, fields) {
    if (err) throw err;
    res.json({
      status: 200,
      data,
      message: `l'utilisateur avec l'id : ${req.params.id} est affiché`
    })
    console.log(data)
  })
});

//POST
app.post('/users', function(req, res) {
  let sql = `INSERT INTO users (pseudo, firstname, lastname) VALUES (?)`;
  let values = [
    req.body.pseudo,
    req.body.firstname,
    req.body.lastname
  ];
  con.query(sql, [values], function(err, data, fields) {
    if (err) throw err;
    res.json({
      status: 200,
      data,
      message: `Ajout d'un utilisateur`
    })
    console.log(data)
  })
});

//DELETE ID
app.delete('/users/:id', function(req, res) {
  let sql = `DELETE FROM users WHERE id = ${req.params.id}`;
  con.query(sql, function(err, data, fields) {
    if (err) throw err;
    res.json({
      status: 200,
      data,
      message: `l'utilisateur avec l'id : ${req.params.id} est supprimé`
    })
    console.log(data)
  })
});

//PUT ID
app.put('/users/:id', function(req, res) {
  let sql = `UPDATE users SET pseudo = ?, firstname = ?, lastname = ? WHERE id = ${req.params.id}`;
  let values = [
    req.body.pseudo,
    req.body.firstname,
    req.body.lastname
  ];  
  con.query(sql, values , function(err, data, fields) {
    if (err) throw err;
    res.send(data)
    res.json({
      status: 200,
      data,
      message: `l'utilisateur avec l'id : ${req.params.id} a été mise à jours`
    })
    console.log(data)
  })
});


app.listen(port)